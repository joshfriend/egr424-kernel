#    
#     EGR 424
#     PROJECT 4 - Multithreaded Kernel
#     Prof. Sterian && Prof. Parikh
#     June 21, 2012
#    
#     STARRING:
#      ______   ______     __     ______     __   __     _____    
#     /\  ___\ /\  == \   /\ \   /\  ___\   /\ "-.\ \   /\  __-.  
#     \ \  __\ \ \  __<   \ \ \  \ \  __\   \ \ \-.  \  \ \ \/\ \ 
#      \ \_\    \ \_\ \_\  \ \_\  \ \_____\  \ \_\\"\_\  \ \____- 
#       \/_/     \/_/ /_/   \/_/   \/_____/   \/_/ \/_/   \/____/ 
#
#                             --- AND ---
#              ______     ______     ______     __   __                             
#             /\  == \   /\  __ \   /\  == \   /\ "-.\ \   
#             \ \  __<   \ \ \/\ \  \ \  __<   \ \ \-.  \  
#              \ \_____\  \ \_____\  \ \_\ \_\  \ \_\\"\_\ 
#               \/_____/   \/_____/   \/_/ /_/   \/_/ \/_/

# Project name
PROJECT = kernel

# Path to StellarisWare drivers/includes
STELLARISWARE=/Developer/StellarisWare

# Path to compiler
PREFIX = /Developer/Cross/arm-cs-tools/bin

# Compiler command
CC = ${PREFIX}/arm-none-eabi-gcc 

# Warnings to ignore
NOWARN = -Wno-main -Wno-strict-aliasing -Wno-unused-function -Wno-comment -Wno-format

# Compiler Flags
CFLAGS = -Wall ${NOWARN} -g -Os -march=armv7-m -mcpu=cortex-m3 -mthumb -mfix-cortex-m3-ldrd -Wl,--gc-sections

# Compile all .c and .S files in this directory ./
SOURCES += $(wildcard ./src/*.c)
SOURCES += $(wildcard ./src/*.S)

# Headers to monitor
HEADERS += $(wildcard ./src/*.h)

# Include library paths
LIBS = -I${STELLARISWARE} -I${STELLARISWARE}/utils -L${STELLARISWARE}/driverlib/gcc-cm3

# Linker flags
LDFLAGS = -Tlinkscript.x -Wl,-Map,map/${PROJECT}.map -Wl,--entry,ResetISR

$(PROJECT): bin/$(PROJECT).elf
	@echo

.PHONY: all
all: $(PROJECT) makefile
	@echo
	@make clean
	@echo
	@make $(PROJECT) || exit $$?;
	@echo
	@make doc || exit $$?;

# Make a binary to load to board
bin/$(PROJECT).elf: $(SOURCES) $(HEADERS) makefile
	@echo "Building\033[0;93m '$(PROJECT)'\033[0m"
	@echo Changed since last build:
	@echo "\033[0;96m$?\033[0m" | tr ' ' '\n'
	@echo
	$(CC) $(CFLAGS) -o $@ $(LIBS) $(LDFLAGS) $(SOURCES) -ldriver-cm3

bin/$(PROJECT).bin: bin/$(PROJECT).elf
	$(PREFIX)/arm-none-eabi-objcopy -O binary $^ $@

bin/$(PROJECT).hex: bin/$(PROJECT).elf
	$(PREFIX)/arm-none-eabi-objcopy -O binary $^ $@

# Assemble a C file
asm/%.S: src/%.c
	$(CC) -S -o $@ $< $(LDFLAGS) $(LIBS)

# Build Doxygen documentation
doc: $(SOURCES) $(HEADERS)
	@echo "Building documentation for\033[0;93m '$(PROJECT)'\033[0m"
	@echo Changed since last build:
	@echo "\033[0;96m$?\033[0m" | tr ' ' '\n'
	@echo
	doxygen Doxyfile

# make a pdf of the documentation
pdf: clean doc
	@make -C doc/latex -f Makefile pdf
	@cp doc/latex/refman.pdf doc/Kernel_Manual.pdf

# Load project to board
load: bin/$(PROJECT).elf
	@echo "Starting GDB to load elf file..."
	@echo "file bin/$(PROJECT).elf\ntarget remote localhost:3333\nmonitor reset halt\nload\nmonitor reset\nquit" > load.gdbcmd
	$(PREFIX)/arm-none-eabi-gdb -x load.gdbcmd
	rm load.gdbcmd

# Start Debugger
# Requires OpenOCD server to be started
debug: bin/$(PROJECT).elf
	@echo "Starting GDB for debug..."
	@echo "file bin/$(PROJECT).elf\ntarget remote localhost:3333\nmonitor reset halt\nload\nb main\ncontinue" > debug.gdbcmd
	$(PREFIX)/arm-none-eabi-gdb -x debug.gdbcmd
	rm debug.gdbcmd

# Start OpenOCD server
ocd::
	@echo "Starting separate (hidden) instance of Terminal.app to host OpenOCD server..."
	open --hide --background /Developer/scripts/openocd.command

# Remove compiled files
clean::
	@echo "Cleaning files matching patterns: *.o *.i *.lst *.out *.elf *bin *.hex *.map *.gdbcmd ..."
	@find . -name \*.elf | xargs rm
	@find . -name \*.bin | xargs rm
	@find . -name \*.gdbcmd | xargs rm
	@find . -name \*.o | xargs rm
	@find . -name \*.hex | xargs rm
	@find . -name \*.i | xargs rm
	@find . -name \*.lst | xargs rm
	@find . -name \*.out | xargs rm
	@find . -name \*.map | xargs rm
	@echo "DONE!"

# vim: noexpandtab  
