# EGR424 Project 4 - Interrupt-driven, Pre-emtive, Multi-threaded Kernel

## Authors

* _Eric Born_
* _Josh Friend_

## Project Requirements
**Due Date: August 7, 2012**

Via [the course website](http://www.andrewsterian.com/424):

* You are to integrate your interrupt-driven device driver from Project #2 and kernel from Project #3 together
with the message-passing infrastructure described below.

* All of the requirements of these projects (e.g., threads run in unprivileged mode with process stack, etc.) still apply.

* You are to provide kernel functions for inter-thread communication using message passing. The API is to be as follows:

        int publish(unsigned channel, unsigned value, unsigned threadCount) {
            /* 
             * Publish the given value to all threads subscribed to the given channel.
             * The threadCount parameter indicates how many threads must receive the
             * published value before this function returns. When threadCount is 0, the
             * function returns immediately. Otherwise, the function blocks (sleeps) until
             * at least threadCount threads receive the given value on the given channel
             * using receive(). The threadCount parameter must be in the range 0 through
             * MAX_THREADS (defined by you).
             *
             * The value parameter is arbitrary and may in fact be a pointer to a data
             * buffer cast to unsigned type. The caller is responsible for managing this
             * buffer and may use threadCount as a method of determining when it is safe
             * to free the buffer.
             *
             * The channel parameter must be in the range 0 through MAX_CHANNEL (defined
             * by you).
             *
             * The return value is 1 to indicate a successful function call or 0 to
             * indicate failure because:
             * - the channel number is invalid, or
             * - the thread count is invalid, or
             * - the system is out of memory and cannot store the publication
             * on the given channel
             */ 
        }

        int subscribe(unsigned channel) {
            /* 
             * Indicate that the active thread is interested in receiving publish()
             * notifications on the given channel. The channel parameter must be in the
             * range 0 through MAX_CHANNEL (defined by you).
             *
             * The return value is 1 to indicate a successful function call or 0 to
             * indicate failure because:
             * - the channel number is invalid
             */
        }

        int receive(unsigned *channel, unsigned *value, unsigned mswait) {
            /* 
             * Wait for a publish() notification on any channel for which the calling
             * thread is subscribed. When this function returns, the pointers channel and
             * value are filled in with the actual channel and value received. The
             * function blocks (sleeps) until a publish() notification is sent on a
             * subscribed channel, or mswait milliseconds elapses, whichever comes first.
             * If mswait is 0, then this function returns immediately with a notification
             * if one was available (in which case channel and value are filled in). If a
             * notification on the given channel was made prior to calling receive(), the
             * function returns immediately with the channel and value filled in,
             * regardless of the value of mswait.
             *
             * The return value of the function is:
             * 0: indicates invalid parameters (either channel or value are null)
             * 1: indicates a notification was received: channel and value are set
             * 2: indicates no notification was received before mswait milliseconds
             * elapsed (i.e., timed out)
             *
             * Set mswait to be ~0 to essentially wait forever (4294967295 ms or 49.7
             * days).
             */
        }
   

* You are to write a demonstration application that uses actual hardware, your device driver, multiple threads,
and message passing for communication between threads. There must be no poll-yield-type loops anywhere.

* All communication between threads is to be through publish/receive message passing, not shared memory
(and global variables are just a form of shared memory, so they are not allowed as a form of inter-thread
communication). Any global variable not declared ’static’ will be met with suspicion (as will an application
that is not broken up logically into multiple .c ﬁles).

**EXTRA CREDIT**: Extra credit will be awarded for a successful implementation that places the ARM processor in a low-power sleep
mode when there are no runnable threads, no messages waiting to be delivered, and an interrupt must occur for a
thread to be woken up. No idle thread! You will have to positively demonstrate that your processor is in a low-power
sleep mode.

## Deliverables

- C/Assembly-code listing of all code that you write. Grading elements will include:

    * Your ability to present “the big picture” – clarity and helpfulness of the comments – proper structure and modularity
    * Spelling
    * Formatting and legibility
    * Correctness, robustness, handling of errors and boundary cases, etc.
- A live demonstration of your working application that exercises all the functionality of your kernel. This demonstration must involve the available hardware of the evaluation board in some way (e.g., pushbuttons, LED’s, serial port).

- A Makefile so that your code can be compiled simply by typing ’make’

## Hardware

* [Texas Instruments LM3S6965 32-bit ARM Cortex-M3 devkit](http://www.digikey.com/product-detail/en/EKC-LM3S6965)

## Software

The core of this project (compiling/debugging) was completed using cross platform tools. Other tools, such as Cygwin, are not cross-platform, but are interchangable.

##### Cross-Platform Tools

* [Sourcery CodeBench Lite Edition 4.6.1](http://www.mentor.com/embedded-software/sourcery-tools/sourcery-codebench/editions/lite-edition/)
* [OpenOCD 0.4.0](http://sourceforge.net/projects/openocd/files/openocd/0.4.0)
* [Sublime Text 2](http://www.sublimetext.com/)
* [Git](http://git-scm.com/)

##### Windows Only

* [Notepad++](http://notepad-plus-plus.org/download/)
* [Cygwin](http://www.cygwin.com/)