/*    
    EGR 424
    Project 4 - Multithreaded Kernel
    Prof. Sterian && Prof. Parikh
    August 7, 2012
    
    STARRING:
     ______   ______     __     ______     __   __     _____    
    /\  ___\ /\  == \   /\ \   /\  ___\   /\ "-.\ \   /\  __-.  
    \ \  __\ \ \  __<   \ \ \  \ \  __\   \ \ \-.  \  \ \ \/\ \ 
     \ \_\    \ \_\ \_\  \ \_\  \ \_____\  \ \_\\"\_\  \ \____- 
      \/_/     \/_/ /_/   \/_/   \/_____/   \/_/ \/_/   \/____/ 

                            --- AND ---
             ______     ______     ______     __   __                             
            /\  == \   /\  __ \   /\  == \   /\ "-.\ \   
            \ \  __<   \ \ \/\ \  \ \  __<   \ \ \-.  \  
             \ \_____\  \ \_____\  \ \_\ \_\  \ \_\\"\_\ 
              \/_____/   \/_____/   \/_/ /_/   \/_/ \/_/ 

*/

//*****************************************************************************
//
//! \addtogroup ringbuffer_api
//! @{
//
//*****************************************************************************

#include <stdlib.h>
#include <stdio.h>
#include "inc/hw_types.h"
#include "ringbuffer.h"
#include "locking.h"

//*****************************************************************************
//
//! \brief Called by a thread to unsubscribe from all channel.
//! \brief Create an instance of a ringBuffer_t object.
//!
//! \param ptRingBuf Pointer to ringBuffer_t object.
//!
//! This function allocates the buffer, initializes the read/write pointers,
//! and initializes the lock_t object of a ringBuffer_t object.
//!
//! \return
//! - \b SUCCESS (1) if write succeeded.
//! - \b FAILURE (0) if write failed.
//
//*****************************************************************************
int createRingBuffer(ringBuffer_t * ptRingBuf)
{
    // ptRingBuf->ulBuffer = (unsigned long *)calloc(size, sizeof(unsigned long));
    if(ptRingBuf->ulBuffer != NULL)
    {
        ptRingBuf->size = sizeof(ptRingBuf->ulBuffer) / sizeof(*(ptRingBuf->ulBuffer));
        ptRingBuf->ulWrite = 0;
        ptRingBuf->ulRead = 0;
        lockInitialize(&ptRingBuf->lock);
    }
    else
    {
        lockAcquire(&UARTLock1);
        iprintf("MALLOC FAIL (%u)\r\n", BUF_SIZE);
        lockRelease(&UARTLock1);
        return FAILURE;
    }

    return SUCCESS;
}

//*****************************************************************************
//
//! \brief Write data to a ringBuffer_t.
//!
//! \param ptRingBuf Pointer to ringBuffer_t object.
//! \param value Data to be written to the ringBuffer_t object.
//!
//! Acquires the lock_t of the ringBuffer_t and writes a value to it.
//!
//! \return
//! - \b SUCCESS (1) if write succeeded.
//! - \b FAILURE (0) if write failed.
//
//*****************************************************************************
int ringBufferWrite(ringBuffer_t * ptRingBuf, unsigned long value)
{
    int status;
    lockAcquire(&ptRingBuf->lock);
    if(isRingBufferFull(ptRingBuf))
    {
        status = FAILURE;
    }
    else
    {
        if(++(ptRingBuf->ulWrite) >= ptRingBuf->size)
        {
            ptRingBuf->ulWrite = 0;
        }

        ptRingBuf->ulBuffer[ptRingBuf->ulWrite] = value;
        status = SUCCESS;
    }
    lockRelease(&ptRingBuf->lock);
    return status;
}

//*****************************************************************************
//
//! \brief Read data from a ringBuffer_t.
//!
//! Acquires the lock_t of the ringBuffer_t and reads a value from it.
//!
//! \param ptRingBuf Pointer to ringBuffer_t object.
//! \param value Pointer to an unsigned long where the read value will be 
//!        stored.
//!
//! \return
//! - \b SUCCESS (1) if write succeeded.
//! - \b FAILURE (0) if write failed.
//
//*****************************************************************************
int ringBufferRead(ringBuffer_t * ptRingBuf, unsigned long *value)
{
    int status;
    lockAcquire(&ptRingBuf->lock);
    if(isRingBufferEmpty(ptRingBuf))
    {
        status = FAILURE;
    }
    else
    {
        if(++(ptRingBuf->ulRead) >= ptRingBuf->size)
        {
            ptRingBuf->ulRead = 0;
        }

        *value = ptRingBuf->ulBuffer[ptRingBuf->ulRead];
        status = SUCCESS;
    }
    lockRelease(&ptRingBuf->lock);
    return status;
}

//*****************************************************************************
//
//! \brief Checks if data can be read from a ringBuffer_t.
//!
//! Checks if the buffer exists and checks the read/write pointers of the
//! ringBuffer_t to see if the buffer can be read from.
//!
//! \param ptRingBuf Pointer to ringBuffer_t object.
//!
//! \return
//! - \b True (1) if data can be read from buffer.
//! - \b False (0) if data cannot be read from buffer.
//
//*****************************************************************************
int isRingBufferEmpty(ringBuffer_t * ptRingBuf)
{
    if(ptRingBuf)
    {
        // lockAcquire(&UARTLock1);
        // iprintf("IE - rd:%u wt:%u sz:%u\r\n", ptRingBuf->ulRead, ptRingBuf->ulWrite, ptRingBuf->size);
        // lockRelease(&UARTLock1);
        return ptRingBuf->ulRead == ptRingBuf->ulWrite;
    }
    else
    {
        lockAcquire(&UARTLock1);
        iprintf("BUFFER NULL\r\n");
        lockRelease(&UARTLock1);
        return TRUE;
    }
}

//*****************************************************************************
//
//! \brief Checks if data can be written to a ringBuffer_t.
//!
//! Checks if the buffer exists and checks the read/write pointers of the
//! ringBuffer_t to see if the buffer can be written to.
//!
//! \return
//! - \b True (1) if data can be written to buffer.
//! - \b False (0) if data cannot be written to buffer.
//
//*****************************************************************************
int isRingBufferFull(ringBuffer_t * ptRingBuf)
{
    if(ptRingBuf)
    {
        unsigned long index = ptRingBuf->ulWrite;
        if(++index >= ptRingBuf->size)
        {
            index = 0;
        }
        // lockAcquire(&UARTLock1);
        // iprintf("IF - rd:%u wt:%u idx:%u sz:%u\r\n", ptRingBuf->ulRead, ptRingBuf->ulWrite, index, ptRingBuf->size);
        // lockRelease(&UARTLock1);
        return ptRingBuf->ulRead == index;
    }
    else
    {
        lockAcquire(&UARTLock1);
        iprintf("BUFFER NULL\r\n");
        lockRelease(&UARTLock1);
        return TRUE;
    }
}

//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************