/*    
    EGR 424
    Project 4 - Multithreaded Kernel
    Prof. Sterian && Prof. Parikh
    August 7, 2012
    
    STARRING:
     ______   ______     __     ______     __   __     _____    
    /\  ___\ /\  == \   /\ \   /\  ___\   /\ "-.\ \   /\  __-.  
    \ \  __\ \ \  __<   \ \ \  \ \  __\   \ \ \-.  \  \ \ \/\ \ 
     \ \_\    \ \_\ \_\  \ \_\  \ \_____\  \ \_\\"\_\  \ \____- 
      \/_/     \/_/ /_/   \/_/   \/_____/   \/_/ \/_/   \/____/ 

                            --- AND ---
             ______     ______     ______     __   __                             
            /\  == \   /\  __ \   /\  == \   /\ "-.\ \   
            \ \  __<   \ \ \/\ \  \ \  __<   \ \ \-.  \  
             \ \_____\  \ \_____\  \ \_\ \_\  \ \_\\"\_\ 
              \/_____/   \/_____/   \/_/ /_/   \/_/ \/_/ 

*/

//*****************************************************************************
//
//! \addtogroup messaging_api
//! @{
//
//*****************************************************************************

#include <stdlib.h>
#include <stdio.h>
#include "inc/hw_types.h"
#include "ringbuffer.h"
#include "kernel.h"
#include "messaging.h"
#include "locking.h"

//*****************************************************************************
//
//! \brief Publish data to the given channel
//!
//! \param channel A number indicating the channel to publish to.
//! \param value The data to be published.
//! \param block \bTRUE if publish should wait for all subscribers to recieve
//!        the message \bFALSE if checking should be skipped.
//!
//! Publish the given value to all threads subscribed to the given channel.
//! The function blocks (sleeps) until all subscribed threads receive the 
//! given value on the given channel using receive().
//! 
//! The value parameter is arbitrary and may in fact be a pointer to a data
//! buffer cast to unsigned type. The caller is responsible for managing this
//! buffer.
//! 
//! The channel parameter must be in the range 0 through MAX_CHANNELS.
//! 
//! \return \b SUCCESS to indicate a successful function call or \b FAILURE to
//! indicate failure because:
//! - the channel number is invalid, or
//! - the system is out of memory and cannot store the publication
//! on the given channel
//
//*****************************************************************************
int publish(unsigned channel, unsigned long value, char block)
{
    //verify channel is valid
    if(channel >= MAX_CHANNELS)
    {
        return FAILURE;
    }

    unsigned subscriptionCount = 0;
    int i;
    int threadCount = getNumThreads();

    for(i = 0; i < threadCount; i++)
    {
        // Check if thread is subscribed to channel
        if(threads[i].subscriptions & (1 << channel))
        {
            subscriptionCount++;

            // Wake thread up, dead thread lose all of their subscriptions
            threads[i].state = AWAKE;

            // Write value to ring buffer
            if(ringBufferWrite(&threads[i].messages[channel], value) == FAILURE)
            {
                lockAcquire(&UARTLock1);
                iprintf("PUBLISH FAIL! rd:%u wt:%u\r\n", threads[gCurrentThread].messages[channel].ulRead
                    ,threads[gCurrentThread].messages[channel].ulWrite);
                lockRelease(&UARTLock1);
                return FAILURE;
            }
        }
    }

    // Allow threads to handle data in buffers
    if(block)
        yield();
    while(subscriptionCount && block)
    {
        for(i = 0; i < threadCount; i++)
        {
            // Check if thread is subscribed to channel
            if(threads[i].subscriptions & (1 << channel))
            {
                // Check if ring buff is empty signifying that thread received data
                if(isRingBufferEmpty(&threads[i].messages[channel]))
                {
                    subscriptionCount--;
                }
            }
        }
    }

    return SUCCESS;
}

//*****************************************************************************
//
//! \brief Receive data on a given channel.
//!
//! \param channel A number indicating the channel to receive from.
//! \param value A pointer to where the received data is stored.
//! \param mswait The number of milliseconds to wait until the operation will
//! complete. Set mswait to be ~0 to essentially wait forever (4294967295 ms or 
//! 49.7 days).
//!
//! Wait for a publish() notification on any channel for which the calling
//! thread is subscribed. When this function returns, the pointers channel and
//! value are filled in with the actual channel and value received. The
//! function blocks (sleeps) until a publish() notification is sent on a
//! subscribed channel, or mswait milliseconds elapses, whichever comes first.
//! if mswait is 0, then this function returns immediately with a notification
//! if one was available (in which case channel and value are filled in). If a
//! notification on the given channel was made prior to calling receive(), the
//! function returns immediately with the channel and value filled in,
//! regardless of the value of mswait.
//!
//! \return
//! \b FAILURE indicates invalid parameters (either channel or value are null)
//! \b SUCCESS indicates a notification was received: channel and value are set
//! \b TIMEOUT indicates no notification was received before mswait milliseconds
//! elapsed (i.e., timed out)
//
//*****************************************************************************
int receive(unsigned *channel, unsigned long *value, unsigned mswait)
{
    if(channel == NULL && value == NULL)
    {
        return FAILURE;
    }

    for(*channel = 0; *channel < MAX_CHANNELS; (*channel)++)
    {
        // Check if thread is subscribed to this channel
        if(threads[gCurrentThread].subscriptions & (1 << *channel))
        {
            // Check for data in buffer
            if(ringBufferRead(&threads[gCurrentThread].messages[*channel], value) == SUCCESS)
            {
                return SUCCESS;
            }
        }
    }

    // Put thread to sleep if no data is recieved before
    threads[gCurrentThread].state = SLEEPING;
    threads[gCurrentThread].wakeTime = gulMSTick + (mswait ? mswait : -1);
    // yield();

    // Operation timed out
    return TIMEOUT;
}

//*****************************************************************************
//
//! \brief Called by a thread to subscribe to a channel.
//!
//! \param channel A number indicating the channel to subscribe to.
//!
//! Indicate that the active thread is interested in receiving publish()
//! notifications on the given channel. The channel parameter must be in the
//! range 0 through MAX_CHANNELS (defined by you).
//
//! \return
//! - \b SUCCESS If subscribe was successful
//! - \b FAILURE If subscribe failed
//
//*****************************************************************************
int subscribe(unsigned channel)
{
    if(channel < MAX_CHANNELS)
    {
        if(createRingBuffer(&threads[gCurrentThread].messages[channel]) == FAILURE)
        {
            // malloc() failed!
            return FAILURE;
        }

        // Subscribe to channel
        threads[gCurrentThread].subscriptions |= (1 << channel);
        lockAcquire(&UARTLock1);
        iprintf("Thread %u subscribed to channel%u, buffer size: %u\r\n", gCurrentThread, channel, BUF_SIZE);
        lockRelease(&UARTLock1);
        return SUCCESS;
    }
    else
    {
        return FAILURE;
    }
}

//*****************************************************************************
//
//! \brief Called by a thread to unsubscribe from all channel.
//!
//! Sets the subscriptions value to 0 to indicate that the thread has no
//! subscriptions.
//
//*****************************************************************************
void unsubscribeAll(void)
{
    int channel;

    for(channel = 0; channel < MAX_CHANNELS; channel++)
    {
        if(threads[gCurrentThread].subscriptions & (1 << channel))
        {
            // free(threads[gCurrentThread].messages[1 << channel].pulBuffer);
        }
    }
    threads[gCurrentThread].subscriptions = 0;
}

//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************