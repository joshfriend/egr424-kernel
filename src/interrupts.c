/*    
    EGR 424
    Project 4 - Multithreaded Kernel
    Prof. Sterian && Prof. Parikh
    August 7, 2012
    
    STARRING:
     ______   ______     __     ______     __   __     _____    
    /\  ___\ /\  == \   /\ \   /\  ___\   /\ "-.\ \   /\  __-.  
    \ \  __\ \ \  __<   \ \ \  \ \  __\   \ \ \-.  \  \ \ \/\ \ 
     \ \_\    \ \_\ \_\  \ \_\  \ \_____\  \ \_\\"\_\  \ \____- 
      \/_/     \/_/ /_/   \/_/   \/_____/   \/_/ \/_/   \/____/ 

                            --- AND ---
             ______     ______     ______     __   __                             
            /\  == \   /\  __ \   /\  == \   /\ "-.\ \   
            \ \  __<   \ \ \/\ \  \ \  __<   \ \ \-.  \  
             \ \_____\  \ \_____\  \ \_\ \_\  \ \_\\"\_\ 
              \/_____/   \/_____/   \/_/ /_/   \/_/ \/_/ 
                                             
*/

//*****************************************************************************
//
//! \addtogroup kernel_api
//! @{
//
//*****************************************************************************

#include <stdio.h>
#include <stdlib.h>
#include "inc/hw_types.h"
#include "inc/hw_nvic.h"
#include "inc/hw_memmap.h"
#include "driverlib/systick.h"
#include "driverlib/interrupt.h"
#include "driverlib/uart.h"
#include "driverlib/timer.h"
#include "led.h"
#include "interrupts.h"
#include "kernel.h"
#include "messaging.h"
#include "rit128x96x4.h"

// threads[] and threadTable[] defined in kernel.h and initialized in kernel.c

// Variables used by interrupt handlers to keep track of framebuffer position
volatile unsigned long ulColumn = 0;
volatile unsigned long ulRow = 0;
volatile unsigned char g_ucRefreshRequest = 0;

// Declare framebuffer of correct size
volatile unsigned char ucImage[(DISPLAY_XPIX / PIX_PER_BYTE) * DISPLAY_YPIX] = {0};

//*****************************************************************************
//
//! \brief The UART interrupt handler
//!
//! Parses recieved imagedata into framebuffer
//!
//! \return None.
//
//*****************************************************************************
void UART0IntHandler(void)
{
    unsigned long ulStatus;
    char c = 0;
    char prev = 0;

    // Get the interrrupt status.
    ulStatus = UARTIntStatus(UART0_BASE, true);

    // Clear the asserted interrupts.
    UARTIntClear(UART0_BASE, ulStatus);
    while(UARTCharsAvail(UART0_BASE))
    {
        c = UARTCharGetNonBlocking(UART0_BASE);

        if(c == START && prev != START)
        {
            // Signal for start of a line
            ulColumn = 0;
        }
        else
        {
            if(prev == START)
            {
                // The byte after the START signal is the row number being recieved next
                ulRow = c;
                if(c == 0)
                {
                    // Send pointer to image data through message api
                    if(publish(VIDEO_DATA, (unsigned long)&ucImage, FALSE) == FAILURE)
                    {
                        lockAcquire(&UARTLock1);
                        iprintf("VIDEO_DATA Publish ERROR!\r\n");
                        lockRelease(&UARTLock1);
                    }
                }
            }
            else
            {
                #ifdef GRAY
                ucImage[(ulRow * (DISPLAY_XPIX / PIX_PER_BYTE)) + ulColumn++] = c;
                #else
                char parsed;
                int i;
                for(i = 0; i < 8; i += 2)
                {
                    // For black and white only frames. Expands two bits into a byte of data
                    // which is the format the OLED driver expects. 
                    parsed = 0;
                    parsed |= (c & (1 << i) ? 0xF0 : 0x00);
                    parsed |= (c & (1 << (i+1)) ? 0x0F : 0x00);
                    ucImage[(ulRow * (DISPLAY_XPIX / PIX_PER_BYTE)) + ulColumn++] = parsed;
                }
                #endif
            }
        }
        // Store previously recieved character
        prev = c;
    }
}

//*****************************************************************************
//
//! \brief Timer0 Exception Handler
//!
//! This function handles Timer0 exceptions. This exceptions simply creates
//! a 1ms heartbeat tick.
//!
//! \return None.
//
//*****************************************************************************
void Timer0IntHandler(void)
{
    // Clear the interrupt
    TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

    // Increment global tick counter
    gulMSTick++;
}

//*****************************************************************************
//
//! \brief SysTick Exception Handler
//!
//! This function handles SysTick exceptions. It saves the state of the
//! previous thread, chooses a new thread, and restores the state of that
//! thread.
//!
//! \return None.
//
//*****************************************************************************
void SysTickHandler(void)
{
    // Save the state of the thread that was interrupted.
    saveThreadState();

    // Call the scheduler to pick next thread.
    scheduler();

    // Restore the state of the thread we just switched to
    restoreThreadState();
}

//*****************************************************************************
//
//! \brief SVC Exception Handler
//!
//! This function is used by the SVC exception handler to perform the actions
//! requested by the process that called the SVC exception.
//!
//! \return None.
//
//*****************************************************************************
__attribute__((naked)) void SVCHandler(void)
{
    asm volatile(
        "@ begin SVCHandler         \n\t"
        "mrs r0, psp                \n\t"
        "cmp r0, #0                 \n\t"   // Check if psp has uninitialized NULL value assigned by startKernel()
        "it eq                      \n\t"
        "mrseq r0, msp              \n\t"   // Use msp if psp == NULL

        "ldr r0, [r0, #24]          \n\t"   // Get return address from exception stackframe (stackptr + 24)
        "sub r0, r0, #2             \n\t"   // Get SVC instruction location (lower halfword)
        "ldrb r0, [r0]              \n\t"   // Load and zero extend lower 8 bits of SVC instruction to r0

        "cmp r0, #0                 \n\t"   // SVC was called to start kernel. DONT save state, just switch context.
        "beq switchContextNoSave    \n\t"   // Call switchContextNoSave()

        "cmp r0, #1                 \n\t"   // SVC was called to switch context. DO save state.
        "beq switchContext          \n\t"   // Call switchContext()
        "@ end SVCHandler           \n\t"
    );
}

//*****************************************************************************
//
//! \brief Performs a context switch
//!
//! This function saves all the registers that are not automatically saved by
//! the exception stack framing into the thread's stack, calls the scheduler
//! to choose a new thread and restores the state of the chosen thread.
//!
//! \return None.
//
//*****************************************************************************
void switchContext(void)
{
    // Save the state of the thread that was interrupted.
    saveThreadState();

    // Set SysTick counter to 0, causing it to reload without
    // generating an exception.
    HWREG(NVIC_ST_CURRENT) = 0;
    // UARTCharPut(UART0_BASE, 'z');

    // Call the scheduler to pick next thread.
    scheduler();

    // Restore the state of the thread we just switched to
    restoreThreadState();
}

//*****************************************************************************
//
//! \brief Performs a context switch without saving previous thread
//!
//! This function performs a context switch without saving the previous thread
//! state. This is done to start the kernel when no previous threads have run.
//!
//! \return None.
//
//*****************************************************************************
void switchContextNoSave(void)
{
    // Set SysTick counter to 0, causing it to reload without
    // generating an exception.
    HWREG(NVIC_ST_CURRENT) = 0;
    // UARTCharPut(UART0_BASE, 'u');

    // Call the scheduler to pick next thread.
    scheduler();

    // Restore the state of the thread we just switched to
    restoreThreadState();
}

//*****************************************************************************
//
//! \brief Preserves the state of the currently selected thread
//!
//! This function saves all the registers that are not automatically saved by
//! the exception stack framing into the thread's stack.
//!
//! - \b r4-r11, \b r13 and \b lr are saved by this function
//!
//! \return None.
//
//*****************************************************************************
void saveThreadState(void)
{
    // Get the PSP
    // Push unsaved registers onto the process stack
    asm volatile (
        "@ begin saveThreadState            \n\t"
        "mrs    r12, psp                    \n\t"
        "stmea  %[ptr]!, {r4-r11, r12, lr}  \n\t"
        "@ end saveThreadState              \n\t"
        : : [ptr] "r" (threads[gCurrentThread].savedRegisters)
    );
}

//*****************************************************************************
//
//! \brief Restores the state of the currently selected thread
//!
//! This function restores all the registers that are not automatically saved
//! by the exception stack framing from the thread's stack.
//!
//! - \b r4-r11, \b r13 and \b lr are restored by this function
//!
//! \return None.
//
//*****************************************************************************
// static inline __attribute__ ((always_inline))
void restoreThreadState(void)
{
    // Pop saved registers off saved process stack
    // Set the PSP to previous location in process stack
    // Set EXC_RETURN to Thread mode using Process Stack
    asm volatile (
        "@ begin restoreThreadState         \n\t"
        "ldmfd  %[ptr]!, {r4-r11, r12, lr}  \n\t"
        "msr    psp, r12                    \n\t"   // Restore psp of new thread
        "movw   lr, #0xFFFD                 \n\t"   // Put bottom half of EXC_RETURN in lr
        "movt   lr, #0xFFFF                 \n\t"   // Put top half of EXC_RETURN in lr
        "bx lr                              \n\t"
        "@ end restoreThreadState           \n\t"
        : : [ptr] "r" (threads[gCurrentThread].savedRegisters)
    );
}

//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************
