#include <stdio.h>
#include "inc/lm3s6965.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/gpio.h"
#include "driverlib/uart.h"
#include "driverlib/interrupt.h"
#include "threads.h"
#include "kernel.h"
#include "locking.h"
#include "rit128x96x4.h"
#include "led.h"
#include "messaging.h"

//*****************************************************************************
//
//! \addtogroup threads
//! @{
//!
//! threads includes userspace thread functions.
//
//*****************************************************************************

//*****************************************************************************
//
//! \brief Prints a line of text over UART.
//!
//! This function obtains a lock on UART0 and prints over the UART, 
//! occasionally yeilding to other threads in the middle of a line. 
//! This serves to demonstrate that the locking is functional as the
//! four print staments all end up on a line in succession.
//!
//! \return None.
//
//*****************************************************************************
THREAD(thread0)
{
    unsigned count;

    lockAcquire(&UARTLock1);
    for (count = 0; count < 10; count++) {
        iprintf("In thread ");
        yield();
        iprintf("%u ", gCurrentThread);
        yield();
        iprintf("-- pass");
        yield();
        iprintf(" %d\r\n", count);
        yield();
    }
    lockRelease(&UARTLock1);
}

//*****************************************************************************
//
//! \brief Prints a line of text over UART.
//!
//! This function obtains a lock on UART0 and prints a line of text over it.
//!
//! \return None.
//
//*****************************************************************************
THREAD(thread1)
{
    unsigned count;

    for (count=0; count < 5; count++) {
        lockAcquire(&UARTLock1);
        iprintf("In thread %u -- pass %d\r\n", gCurrentThread, count);
        lockRelease(&UARTLock1);
        yield();
    }
}

//*****************************************************************************
//
//! \brief Prints a line of text over UART.
//!
//! This function obtains a lock on UART0 and prints a line of text over it.
//!
//! \return None.
//
//*****************************************************************************
THREAD(thread2)
{
    unsigned count;

    for (count=0; count < 20; count++) {
        lockAcquire(&UARTLock1);
        iprintf("In thread %u -- pass %d\r\n", gCurrentThread, count);
        lockRelease(&UARTLock1);
        yield();
    }
}

//*****************************************************************************
//
//! \brief Checks the status of the select push button.
//!
//! This function Check the select push button's status and will print any 
//! changes over UART once it is able to obtain a lock on UART0.
//!
//! \return None.
//
//*****************************************************************************
THREAD(checkSelectButton)
{
    unsigned state = 10;
    unsigned pressed = 0;
    unsigned prev_pressed = 1;
    
    while(1)
    {
        // "Shift register" to debounce
        if(GPIOPinRead(GPIO_PORTF_BASE, GPIO_PIN_1))
        {
            if(state < 10)
            {
                state++;
            }
        }
        else
        {
            if(state > 0)
            {
                state--;
            }
        }

        // Determine debounced button state
        if(state == 10)
        {
            pressed = 0;
        }
        else if(state == 0)
        {
            pressed = 1;
        }

        // Print button state
        if(pressed != prev_pressed)
        {
            if(pressed)
            {
                lockAcquire(&UARTLock1);
                iprintf("Select Button Pressed...\r\n");
                lockRelease(&UARTLock1);
            }
            else
            {
                lockAcquire(&UARTLock1);
                iprintf("Select Button Released...\r\n");
                lockRelease(&UARTLock1);
            }
            prev_pressed = pressed;
        }

        yield();
    }
}

//*****************************************************************************
//
//! \brief This thread does nothing... forever.
//!
//! This function will enter sleep mode continually and wait for the SysTick
//! ISR to wake and perform a context switch.
//!
//! \note Uses the ``WFI`` instruction to "Wait for interrupt". See details
//! in ARM Architecture Ref. Manual A6.7.152.
//!
//! \return None.
//
//*****************************************************************************
THREAD(idleThread)
{
    while(1)
    {
        /*
         From ARM-ARM A6.7.152:
         Wait For Interrupt is a hint instruction. 
         It suspends execution, in the lowest power state available consistent
         with a fast wakeup without the need for software restoration, until a
         reset, asynchronous exception or other event occurs. 
         */

        // Enter sleep mode
        asm volatile("wfi");

        /*
         Enable this (and slow down the systick isr!) to verify that this
         Loop does not execute more than once per SysTick.
         */
        // lockAcquire(&UARTLock1);
        // iprintf("WFI\n");
        // lockRelease(&UARTLock1);
        
    }
}

//*****************************************************************************
//
//! \brief This thread toggles the status LED.
//!
//! This function Toggles the stuatus LED and then calls yeild() a set number
//! of times.
//!
//! \return None.
//
//*****************************************************************************
THREAD(blinkThread)
{
    unsigned long i = 1000;
    while(i--)
    {
        toggleStatusLED();
        yield();
    }
}

//*****************************************************************************
//
//! \brief This thread prints a line of text to the OLED display.
//!
//! This function prints a line of text to the OLED display a set number of
//! times before yeilding.
//!
//! \return None.
//
//*****************************************************************************
THREAD(OLEDThread)
{
    unsigned count;
    char buf[64];
    for(count = 0; count <= 1000; count++)
    {
        sprintf(buf, "OLED Thread: %d", count);
        lockAcquire(&OLEDLock);
        RIT128x96x4StringDraw(buf, 0,  88, 15);
        lockRelease(&OLEDLock);
        yield();
    }
    lockAcquire(&OLEDLock);
    RIT128x96x4StringDraw("OLED Thread: DONE!", 0,  88, 15);
    lockRelease(&OLEDLock);
}

//*****************************************************************************
//
//! \brief This thread checks its privilege mode and what stack is in use.
//!
//! This function checks the privilege mode and process stack in three ways:
//! - Read the ``msp`` and check if it is ``NULL``. Unprivileged threads can't 
//! read the ``msp`` so if a thread reads ``msp`` as ``NULL``, it uses the
//! ``psp`` instead and is unprivileged.
//! - Read the ``control`` register and check the privilege and stack select
//! bits. ``control[0]`` selects the privilege mode of threads and
//! ``control[1]`` selects the stack to be used in thread mode.
//! - Attempting to disable interrupts. unprivileged threads cannot make
//! changes to the NVIC, therefore, a call to IntMasterDisable() should not 
//! stop the kernel
//!
//! \note This thread is best left deactivated as it 'spams' the console with
//! info since it continuously checks privilege status.
//!
//! \return None.
//
//*****************************************************************************
THREAD(privilegeTest)
{
    unsigned control_reg;
    unsigned long * main_stack;

    while(1)
    {
        // Attempt to retrieve main stack pointer and control register
        asm volatile(
            "mrs %[main_stack], msp         \n\t"   // Try to get MSP
            "mrs %[control_reg], control    \n\t"   // Get control register
            : [main_stack] "=r" (main_stack),
              [control_reg] "=r" (control_reg)
        );

        // Print info to console
        lockAcquire(&UARTLock1);
        if(main_stack == (unsigned long *)NULL)
            // NULL is an invalid value for the stack pointer so if we read that
            // as the msp value, we are unprivileged
            iprintf("msp = NULL -> unprivileged\r\n");
        else
            iprintf("msp = %i -> privileged\r\n", main_stack);

        // Reference: Page B1-11 of ARM-ARM
        // Check privilege setting bit
        if(control_reg & 0x01)
            iprintf("control[0] = 1  -> Thread mode is unprivileged\r\n");
        else
            iprintf("control[0] = 0  -> Thread mode is privileged\r\n");

        // Check selected stack pointer bit
        if(control_reg & 0x02)
            iprintf("control[1] = 1  -> SP_Process is current stack\r\n\n");
        else
            iprintf("control[1] = 0  -> SP_Main is current stack\r\\n");
        lockRelease(&UARTLock1);

        // Attempt to disable interrupts
        IntMasterDisable();
    }
}

//*****************************************************************************
//
//! \brief Tests context switch time
//!
//! When run alone, this will test the context switching time by toggling the
//! LED state.
//!
//! \return None.
//
//*****************************************************************************
THREAD(contextSwitchTimeTester)
{
    while(1)
    {
        // The LED Pulses ON while switching context...
        GPIO_PORTF_DATA_R |= 0x01;
        yield();
        GPIO_PORTF_DATA_R &= ~0x01;
    }
}

//*****************************************************************************
//
//! \brief Tests the publish() function
//!
//! Publishes a sequence of values to all channels.
//!
//! \return None.
//
//*****************************************************************************
THREAD(publishThread)
{
    unsigned value, channel;

    for(value = 2; value < 100; value++)
    {
        for(channel = 2; channel < MAX_CHANNELS; channel++)
        {
            switch(publish(channel, value, TRUE))
            {
                case FAILURE:
                    lockAcquire(&UARTLock1);
                    iprintf("Publish FAILURE! %u on ch%u\r\n\r\n", value, channel);
                    lockRelease(&UARTLock1);
                    break;
                case SUCCESS:
                    lockAcquire(&UARTLock1);
                    iprintf("Published '%u' on channel%u\r\n\r\n", value, channel);
                    lockRelease(&UARTLock1);
                    break;
            }
        }
    }
}

//*****************************************************************************
//
//! \brief Tests the receive() function
//!
//! Subscribes to all channels and prints the messages it recieves on each.
//!
//! \return None.
//
//*****************************************************************************
THREAD(recieveAllThread)
{
    unsigned channel;
    unsigned long value;

    for(channel = 0; channel < MAX_CHANNELS; channel++)
    {
        if(subscribe(channel) == FAILURE)
        {
            lockAcquire(&UARTLock1);
            iprintf("SUBSCRIBE FAILURE: ch%u!\r\n", channel);
            lockRelease(&UARTLock1);
            return;
        }
    }

    while(1)
    {
        switch(receive(&channel, &value, 10))
        {
            case FAILURE:
                lockAcquire(&UARTLock1);
                iprintf("Message recieve FAILURE!\r\n");
                lockRelease(&UARTLock1);
                break;
            case SUCCESS:
                lockAcquire(&UARTLock1);
                switch(channel)
                {
                    case KERNEL_MESSAGES:
                        iprintf("KERNEL MESSAGE '%u'\r\n", value);
                        break;
                    default:
                        iprintf("Recieved '%u' on channel%u\r\n", value, channel);
                        break;
                }
                lockRelease(&UARTLock1);
                break;
            case TIMEOUT:
                // lockAcquire(&UARTLock1);
                // iprintf("TIMEOUT\r\n");
                // lockRelease(&UARTLock1);
                yield();
                break;
        }
    }
}

//*****************************************************************************
//
//! \brief Draws video data from UART0 on the OLED display
//!
//! Subscribes to video channel and prints frames as they are sent over the
//! message buffer.
//!
//! \return None.
//
//*****************************************************************************
THREAD(drawImageThread)
{
    unsigned channel;
    unsigned long value;

    if(subscribe(VIDEO_DATA) == FAILURE)
    {
        lockAcquire(&UARTLock1);
        iprintf("SUBSCRIBE FAILURE: ch%u!\r\n", channel);
        lockRelease(&UARTLock1);
        return;
    }

    // Send start signal
    UARTCharPutNonBlocking(UART0_BASE, XON);

    // Block until python app starts
    while(UARTCharGet(UART0_BASE) != XON)
        yield();

    while(1)
    {
        switch(receive(&channel, &value, 1000))
        {
            case FAILURE:
                lockAcquire(&UARTLock1);
                iprintf("Message recieve FAILURE!\r\n");
                lockRelease(&UARTLock1);
                break;
            case SUCCESS:
                if(channel == KERNEL_MESSAGES)
                {
                    lockAcquire(&UARTLock1);
                    iprintf("KERNEL MESSAGE '%u'\r\n", value);
                    lockRelease(&UARTLock1);
                }
                else if(channel == VIDEO_DATA)
                {
                    if((unsigned char *)value != NULL)
                    {
                        lockAcquire(&UARTLock0);
                        // iprintf("Recieved '%u' on channel%u\r\n", value, channel);
                        
                        lockAcquire(&OLEDLock);
                        // Refresh display
                        // Send flow control signal
                        UARTCharPutNonBlocking(UART0_BASE, XOFF);

                        RIT128x96x4ImageDraw((unsigned char *)value, 0, 0, DISPLAY_XPIX, DISPLAY_YPIX);

                        // Send flow control signal
                        UARTCharPutNonBlocking(UART0_BASE, XON);
                        lockRelease(&OLEDLock);
                        lockRelease(&UARTLock0);
                    }
                }
                else
                {
                    lockAcquire(&UARTLock1);
                    iprintf("Recieved '%u' on channel%u\r\n", value, channel);
                    lockRelease(&UARTLock1);
                }
                break;
            case TIMEOUT:
                // lockAcquire(&UARTLock1);
                // iprintf("TIMEOUT\r\n");
                // lockRelease(&UARTLock1);
                yield();
                break;
        }
    }
}

//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************