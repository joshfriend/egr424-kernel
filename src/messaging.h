/*    
    EGR 424
    Project 4 - Multithreaded Kernel
    Prof. Sterian && Prof. Parikh
    August 7, 2012
    
    STARRING:
     ______   ______     __     ______     __   __     _____    
    /\  ___\ /\  == \   /\ \   /\  ___\   /\ "-.\ \   /\  __-.  
    \ \  __\ \ \  __<   \ \ \  \ \  __\   \ \ \-.  \  \ \ \/\ \ 
     \ \_\    \ \_\ \_\  \ \_\  \ \_____\  \ \_\\"\_\  \ \____- 
      \/_/     \/_/ /_/   \/_/   \/_____/   \/_/ \/_/   \/____/ 

                            --- AND ---
             ______     ______     ______     __   __                             
            /\  == \   /\  __ \   /\  == \   /\ "-.\ \   
            \ \  __<   \ \ \/\ \  \ \  __<   \ \ \-.  \  
             \ \_____\  \ \_____\  \ \_\ \_\  \ \_\\"\_\ 
              \/_____/   \/_____/   \/_/ /_/   \/_/ \/_/ 
                                             
*/

//*****************************************************************************
//
//! \file messaging.h
//!
//! Header for kernel.c containing api function delcarations and variables.
//
//*****************************************************************************

#ifndef MESSAGING_H
#define MESSAGING_H

#include "inc/hw_types.h"

//! \def
#define WAIT_FOREVER    0

//! \def
#define FAILURE         0
//! \def
#define SUCCESS         1
//! \def
#define TIMEOUT         2

//! \def
#define KERNEL_MESSAGES 0
//! \def
#define VIDEO_DATA      1

//! \def
#define REFRESH         1

extern int publish(unsigned channel, unsigned long value, char block);
extern int receive(unsigned *channel, unsigned long *value, unsigned mswait);
extern int subscribe(unsigned channel);
extern void unsubscribeAll(void);

#endif