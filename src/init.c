/*    
    EGR 424
    Project 4 - Multithreaded Kernel
    Prof. Sterian && Prof. Parikh
    August 7, 2012
    
    STARRING:
     ______   ______     __     ______     __   __     _____    
    /\  ___\ /\  == \   /\ \   /\  ___\   /\ "-.\ \   /\  __-.  
    \ \  __\ \ \  __<   \ \ \  \ \  __\   \ \ \-.  \  \ \ \/\ \ 
     \ \_\    \ \_\ \_\  \ \_\  \ \_____\  \ \_\\"\_\  \ \____- 
      \/_/     \/_/ /_/   \/_/   \/_____/   \/_/ \/_/   \/____/ 

                            --- AND ---
             ______     ______     ______     __   __                             
            /\  == \   /\  __ \   /\  == \   /\ "-.\ \   
            \ \  __<   \ \ \/\ \  \ \  __<   \ \ \-.  \  
             \ \_____\  \ \_____\  \ \_\ \_\  \ \_\\"\_\ 
              \/_____/   \/_____/   \/_/ /_/   \/_/ \/_/ 
                                             
*/

//*****************************************************************************
//
//! \addtogroup config_api
//! @{
//!
//! Config_api contains functions for initializing devkit peripherals.
//
//*****************************************************************************

#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_sysctl.h"
#include "driverlib/systick.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "driverlib/timer.h"
#include "init.h"
#include "rit128x96x4.h"

//*****************************************************************************
//
//! \brief Configure UART
//!
//! \param baudrate The desired debug peripheral baudrate
//!
//! This function:
//! - Enables the UART peripherals
//! - Configures the GPIO pins used by the UARTs.
//! - Sets the UART mode to given baudrate, 8-N-1
//!
//! \return None.
//
//*****************************************************************************
void UARTConfig(unsigned int baudrate)
{
    // Enable HW peripherals for UART
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

    // Set GPIO A0 and A1 as UART pins.
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

    // Configure the UART for 115,200, 8-N-1 operation.
    UARTConfigSetExpClk(UART0_BASE, SysCtlClockGet(), 230400,
                        (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                         UART_CONFIG_PAR_NONE));

    // Enable the UART interrupt.
    IntEnable(INT_UART0);

    // Configure UART interrupt behavior:
    // * Interrupt on RX timeout
    // * Interrupt on RX FIFO 3/4 filled
    UARTIntEnable(UART0_BASE, UART_INT_RX | UART_INT_RT);
    UARTFIFOEnable(UART0_BASE);
    UARTFIFOLevelSet(UART0_BASE, UART_FIFO_TX6_8, UART_FIFO_RX6_8);

    // ---------------------------------------------------------------

    // Enable HW peripherals for UART1
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART1);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);

    // Set GPIO D2 and D3 as UART1 pins.
    GPIOPinTypeUART(GPIO_PORTD_BASE, GPIO_PIN_2 | GPIO_PIN_3);

    // Configure the UART1 for given baudrate, 8-N-1 operation.
    UARTConfigSetExpClk(UART1_BASE, SysCtlClockGet(), baudrate,
                       (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                        UART_CONFIG_PAR_NONE));
}

//*****************************************************************************
//
//! \brief Configure SysTick timer
//!
//! This function:
//! - Enables the SysTick peripheral
//! - Enables the SysTick interrupt
//! - Sets the SysTick period to 1ms
//!
//! \return None.
//
//*****************************************************************************
void SysTickConfig(void)
{
    // Configure SYSTICK interrupt for 1ms period
    SysTickEnable();
    SysTickIntEnable();
    SysTickPeriodSet(SysCtlClockGet()/1000);
}

//*****************************************************************************
//
//! \brief Configure OLED display
//!
//! This function configures the OLED screen and prints a welcome message.
//!
//! \return None.
//
//*****************************************************************************
void DisplayConfig(void)
{
    // Initialize the OLED display and write status.
    RIT128x96x4Init(3500000);
    RIT128x96x4StringDraw("J. Friend && E. Born", 0,  0, 15);
}

//*****************************************************************************
//
//! \brief Configure Buttons and LEDs
//!
//! This function configures the GPIO registers to enable the buttons.
//!
//! \return None.
//
//*****************************************************************************
void ButtonConfig(void)
{
    // Enable the GPIO port that is used for the buttons
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);

    // Set pin directions
    GPIOPinTypeGPIOInput(GPIO_PORTE_BASE,
                         GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
    GPIOPinTypeGPIOInput(GPIO_PORTF_BASE, GPIO_PIN_1);

    // Enable pullups
    GPIOPadConfigSet(GPIO_PORTE_BASE,
                     GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3,
                     GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
    GPIOPadConfigSet(GPIO_PORTF_BASE, GPIO_PIN_1, GPIO_STRENGTH_2MA,
                     GPIO_PIN_TYPE_STD_WPU);
}

//*****************************************************************************
//
//! \brief Configure Timer peripherals
//!
//! This function configures the Timer0 peripheral for a 1ms tick
//!
//! \return None.
//
//*****************************************************************************
void Timer0Config(void)
{
    IntMasterEnable();
    // Enable the peripherals used by this example.
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);

    // Configure TIMER0 as a 32-bit periodic timer
    TimerConfigure(TIMER0_BASE, TIMER_CFG_PERIODIC);
    TimerLoadSet(TIMER0_BASE, TIMER_A, SysCtlClockGet() / 1000);

    // Setup the interrupts for TIMER0
    IntEnable(INT_TIMER0A);
    TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

    // Enable TIMER0
    TimerEnable(TIMER0_BASE, TIMER_A);
}

//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************
