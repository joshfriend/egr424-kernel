/*    
    EGR 424
    Project 4 - Multithreaded Kernel
    Prof. Sterian && Prof. Parikh
    August 7, 2012
    
    STARRING:
     ______   ______     __     ______     __   __     _____    
    /\  ___\ /\  == \   /\ \   /\  ___\   /\ "-.\ \   /\  __-.  
    \ \  __\ \ \  __<   \ \ \  \ \  __\   \ \ \-.  \  \ \ \/\ \ 
     \ \_\    \ \_\ \_\  \ \_\  \ \_____\  \ \_\\"\_\  \ \____- 
      \/_/     \/_/ /_/   \/_/   \/_____/   \/_/ \/_/   \/____/ 

                            --- AND ---
             ______     ______     ______     __   __                             
            /\  == \   /\  __ \   /\  == \   /\ "-.\ \   
            \ \  __<   \ \ \/\ \  \ \  __<   \ \ \-.  \  
             \ \_____\  \ \_____\  \ \_\ \_\  \ \_\\"\_\ 
              \/_____/   \/_____/   \/_/ /_/   \/_/ \/_/ 
                                             
*/

//*****************************************************************************
//
//! \addtogroup kernel_api
//! @{
//!
//! Kernel_api includes functions for the creation and removal of threads.
//
//*****************************************************************************

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "inc/hw_types.h"
#include "driverlib/sysctl.h"
#include "driverlib/systick.h"
#include "driverlib/interrupt.h"
#include "rit128x96x4.h"
#include "led.h"
#include "kernel.h"
#include "locking.h"
#include "messaging.h"

thread_t threadTable[] = {
    idleThread
    // ,thread0
    // ,thread1
    // ,thread2
    // ,blinkThread
    // ,checkSelectButton
    // ,OLEDThread
    ,publishThread
    ,recieveAllThread
    ,drawImageThread
    // ,recieveEvenThread
    // ,privilegeTest          // Best left OFF as it 'spams' console with info messages.
    // contextSwitchTimeTester // This one will ONLY work if it is the ONLY thread running!
};

threadStruct_t threads[NUM_THREADS];

// The index of the currently running thread
int gCurrentThread = 0;

// Global millisecond tick
unsigned long gulMSTick = 0;

// Global OLED stringdraw buffer
char buf[64];

//*****************************************************************************
//
//! \brief Clear a thread's stack
//!
//! \param stackptr The stack pointer of the thread
//!
//! This function clears the stackspace of a thread to 0xDEADBEEF
//!
//! \return None.
//
//*****************************************************************************
void clearStack(char *stackptr)
{
    int i;
    for(i = 0; i < STACK_SIZE; i++)
    {
        *(stackptr - i) = 0xAA;
    }
}

//*****************************************************************************
//
//! \brief Determine how much stack space was used
//!
//! \param stackptr The stack pointer of the thread
//!
//! This function starts from the bottom of a thread's stack and searches for
//! the locations which contain a value that was different from the unique
//! initializer value, 0xDEADBEEF.
//!
//! \return None.
//
//*****************************************************************************
int checkStackFreeSpace(char *stackptr)
{
    int i;
    for(i = STACK_SIZE - 1; i != 0; i--)
    {
        if(*(stackptr - i) != 0xAA)
            break;
    }
    return STACK_SIZE - i;
}

//*****************************************************************************
//
//! \brief Tell the scheduler to perform a context switch
//!
//! This function generates an SVC Exception indicated that the kernel should
//! perform a context switch, saving the state of the previous thread first.
//!
//! \return None.
//
//*****************************************************************************
void yield(void)
{
    // iprintf("yield()\r\n");
    // Generate SVC exception to signal context switch to next thread
    asm volatile (
        "svc #1       \n\t"   // call SVC with reason #1
    );
}

//*****************************************************************************
//
//! \brief Initialize a thread
//!
//! \param buf The registerBuffer buffer associated with the thread
//! \param stack The stack pointer for the thread
//!
//! This function is implemented in assembly language. It sets up the
//! initial registerBuffer but with our own values for the stack and LR 
//! (always set to threadStarter() for each thread).
//!
//! \return None.
//
//*****************************************************************************
void initThread(registerBuffer buffer, char *stack)
{
    // iprintf("initThread()\r\n");
    int filler = 0xFFFF;
    asm volatile(
        "@ begin initThread             \n\t"
        /*
        EXCEPTION STACKFRAME:
                +−−−−−−−−−−−−−−−−−−−+
        High    |       APSR        |  --> xPSR to set Thumb mode and stack alignment
                +−−−−−−−−−−−−−−−−−−−+
                | AddressToReturnTo |  --> threadStarter()
                +−−−−−−−−−−−−−−−−−−−+
                |      LR/R14       |  --> threadDestroyer()
                +−−−−−−−−−−−−−−−−−−−+
                |        R12        |
                +−−−−−−−−−−−−−−−−−−−+
                |        R3         |
                +−−−−−−−−−−−−−−−−−−−+
                |        R2         |
                +−−−−−−−−−−−−−−−−−−−+
                |        R1         |
                +−−−−−−−−−−−−−−−−−−−+
        Low     |        R0         |
                +−−−−−−−−−−−−−−−−−−−+

        We need to put a fake one of these on the process stack so that the first time
        an SVC call switches to this process, it will return to threadStarter to 
        kick off the process.
     
        Set value of the EPSR
        EPSR[9] = 1 to set Stack alignment to 8 bytes as required by AAPCS (ARM-ARM B1-24)
        EPSR[24] = 1 to indicate return to Thumb mode (must always be set in CM3 - see ARM-ARM B1-9)
        see: http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.dui0553a/CHDBIBGJ.html
        */
        "movw r2, #0x0000               \n\t"
        "movt r2, #0x0100               \n\t"
        "stmfd %[stack]!, {r2}          \n\t"

        // Push AddressToReturnTo on process stack. When returning to thread mode, the code
        // will begin execution here. We want this to be threadStarter() initially.
        "stmfd %[stack]!, {%[threadStarter]}    \n\t"

        // Push LR on process stack. When the thread completes, this is where it will
        // return to. We want this to be the teardown function, threadDestroyer().
        // Perhaps this is redundant since threadStarter() calls threadDestroyer()?
        "stmfd %[stack]!, {%[threadDestroyer]}  \n\t"

        // Push r0-r3, and r12 on process stack like an exception would.
        // The values are not important.
        "subs %[stack], #20             \n\t"
        // "stmfd %[stack]!, {%[filler]}   \n\t"   // r12  |
        // "stmfd %[stack]!, {%[filler]}   \n\t"   // r3   |
        // "stmfd %[stack]!, {%[filler]}   \n\t"   // r2   |
        // "stmfd %[stack]!, {%[filler]}   \n\t"   // r1   |
        // "stmfd %[stack]!, {%[filler]}   \n\t"   // r0   V

        //---------------------------------------------------------------------------------------

        /*
        The registerBuffer buffer saves all the information that the exception stackframe does
        not. This includes r4-r11, r13/sp and lr.

        Save r4-r11 on process stack. Values don't matter. We just have to 
        store some stuff to fake it the first time to get the pointer in the
        right spot. Initially, the pointer to this array is at element 0, so 
        we use 'stmea' instead of 'stmfd'.
        */
        "stmea %[buffer]!, {r4-r11}     \n\t"

        // Also have to save the process stack pointer (psp)
        "stmea %[buffer]!, {%[stack]}   \n\t"

        /*
        Store proper value in LR (EXEC_RETURN)
        EXEC_RETURN[31:4] must be 1 or result is UNPREDICTABLE
        EXEC_RETURN[3:0] should be 0b1101 so that when the exception returns, return state is
        derived from the Process Stack and execution will use the Process Stack in Thread Mode.
        (ARM-ARM B1-26)
        */
        "movw r2, #0xFFFD               \n\t"
        "movt r2, #0xFFFF               \n\t"
        "stmea %[buffer]!, {r2}         \n\t"
        "@ end initTread                \n\t"
        :                                           // There are no outputs
        : [threadStarter] "r" (threadStarter),      // Function that starts the thread
        [threadDestroyer] "r" (threadDestroyer),    // Function that ends the thread
        [buffer] "r" (buffer),                      // Context switch savebuffer
        [stack] "r" (stack),                        // The stack for the thread
        [filler] "r" (filler)                       // Initial value of the saved registers
        : "r2"                                      // We modify r2, so it is 'clobbered'
    );
}

//*****************************************************************************
//
//! \brief Start a thread
//!
//! This function starts the thread by calling its function. When the thread
//! finishes, threadDestroyer() is called to perform teardown.
//!
//! \return None.
//
//*****************************************************************************
void threadStarter(void)
{
    // lockAcquire(&UARTLock1);
    // iprintf("threadStarter(): %i\r\n", gCurrentThread);
    // lockRelease(&UARTLock1);

    // Call the entry point for this thread. The next line returns
    // only when the thread exits.
    (*(threadTable[gCurrentThread]))();

    // Report free stack space
    int freespace = checkStackFreeSpace(threads[gCurrentThread].stack);
    lockAcquire(&UARTLock1);
    iprintf("Thread#%i - Free: %i bytes - Used: %i bytes\r\n", gCurrentThread, freespace, STACK_SIZE - freespace);
    lockRelease(&UARTLock1);

}

//*****************************************************************************
//
//! \brief Ends a thread
//!
//! This function sets the active state of a thread to FALSE and calls the
//! scheduler. The scheduler will free() the stack.
//!
//! \return None.
//
//*****************************************************************************
void threadDestroyer(void)
{
    lockAcquire(&OLEDLock);
    sprintf(buf, "threadDestroyer(): %i", gCurrentThread);
    RIT128x96x4StringDraw(buf, 0,  24, 15);
    lockRelease(&OLEDLock);
    // Mark thread as inactive
    threads[gCurrentThread].state = DEAD;

    // Unsubscribe thread form all messages
    unsubscribeAll();

    // This will call the scheduler which will see that the thread is inactive
    // and will free() the stack.
    yield();
}

//*****************************************************************************
//
//! \brief Thread creator
//!
//! This function initializes all the user threads by allocating a stack and
//! mark them as active.
//!
//! \return
//! - \b 0 if process failed
//! - \b 1 if process was successful
//
//*****************************************************************************
int createThreads(void)
{
    int i;
    for (i = 0; i < NUM_THREADS; i++) {
        iprintf("Marking thread%i as active\r\n", i);
        
        // Mark thread as awake
        threads[i].state = AWAKE;

        iprintf("\tAllocating stack\r\n");
        // Allocate stack
        threads[i].stack = (char *)malloc(STACK_SIZE) + STACK_SIZE;

        if (threads[i].stack == NULL) {
            // Return with failure
            iprintf("FAILURE\r\n");
            return 0;
        }

        iprintf("\tClearing stack\r\n");
        // Set stack data to known value (0xDEADBEEF)
        clearStack(threads[i].stack);

        // Call initThread() to set threadStarter/threadDestroyer
        // In proper places inside fake exception stackframe
        initThread(threads[i].savedRegisters, threads[i].stack);
    }

    // Return success
    return 1;
}

//*****************************************************************************
//
//! \brief Returns the number of threads in the system.
//!
//! \return The number of threads in the system.
//
//*****************************************************************************
int getNumThreads(void)
{
    return NUM_THREADS;
}


//*****************************************************************************
//
//! \brief Kernel Task Scheduler
//!
//! This function chooses the next thread from the table of threads. If the
//! thread is not active anymore, the stack space for that thread is freed.
//! This round-robin scheduling is repeated until no active threads remain.
//!
//! \return None.
//
//*****************************************************************************
void scheduler(void)
{
    int i = NUM_THREADS - 1;
    int activeThreads = 0;

    // Loop through all threads till we find one that's awake
    while(i--)
    {
        // Choose next thread with round-robin style
        if(++gCurrentThread >= NUM_THREADS)
        {
            gCurrentThread = 1;
        }

        switch(threads[gCurrentThread].state)
        {
            case AWAKE:
                // Active thread found, nothing to do here...
                return;
            case SLEEPING:
                activeThreads++;
                // Check if thread is ready to be woken up
                if(threads[gCurrentThread].wakeTime <= gulMSTick)
                {
                    threads[gCurrentThread].state = AWAKE;
                    return;
                }
                break;
            case DEAD:
                // Thread is done, free its stack
                if(threads[gCurrentThread].stack != NULL)
                {
                    free(threads[gCurrentThread].stack - STACK_SIZE);
                    threads[gCurrentThread].stack = NULL;
                }
                break;
            default:
                // lockAcquire(&UARTLock1);
                iprintf("scheduler(): Unknown thread state: %u\r\n", threads[gCurrentThread].state);
                // lockRelease(&UARTLock1);
                break;
        }
    }

    if(activeThreads)
    {
        // All other threads are sleeping but at least 1 is still active
        // Run idle thread while waiting for thread to wake up
        gCurrentThread = 0;
        return;
    }

    lockAcquire(&UARTLock1);
    iprintf("scheduler(): ALL THREADS DONE\r\n");
    lockRelease(&UARTLock1);

    lockAcquire(&OLEDLock);
    RIT128x96x4StringDraw("All threads done!", 0,  32, 15);
    lockRelease(&OLEDLock);

    // ALL threads have completed if we get here
    SysTickIntDisable();
    SysTickDisable();

    IntMasterDisable();

    setStatusLED(1);

    exit(0);
}

//*****************************************************************************
//
//! \brief Starts the Kernel
//!
//! This function sets the current thread number, enables interrupts globally
//! and sets the privilege mode of threads to unprivileged. The SVC is then
//! called which will run the scheduler.
//!
//! \return None.
//
//*****************************************************************************
void startKernel(void)
{
    // Will be incremented by scheduler
    gCurrentThread = -1;

    IntMasterEnable();

    asm volatile(
        "mov r0, #0         \n\t"
        "msr psp, r0        \n\t"   // Set psp to NULL (indicate that psp has not been used yet)
        "mov r0, #1         \n\t"
        "msr control, r0    \n\t"   // Set thread unprivileged mode. Exception return handling
                                    // will determine what stack is used on re-entry.
        "isb                \n\t"   // Clear pipeline
        "svc #0             \n\t"   // Call SVC to switch context, without saving prev. thread
        : : : "r0"
    );
}

//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************
