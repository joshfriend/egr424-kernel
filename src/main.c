/*    
    EGR 424
    Project 4 - Multithreaded Kernel
    Prof. Sterian && Prof. Parikh
    August 7, 2012
    
    STARRING:
     ______   ______     __     ______     __   __     _____    
    /\  ___\ /\  == \   /\ \   /\  ___\   /\ "-.\ \   /\  __-.  
    \ \  __\ \ \  __<   \ \ \  \ \  __\   \ \ \-.  \  \ \ \/\ \ 
     \ \_\    \ \_\ \_\  \ \_\  \ \_____\  \ \_\\"\_\  \ \____- 
      \/_/     \/_/ /_/   \/_/   \/_____/   \/_/ \/_/   \/____/ 

                            --- AND ---
             ______     ______     ______     __   __                             
            /\  == \   /\  __ \   /\  == \   /\ "-.\ \   
            \ \  __<   \ \ \/\ \  \ \  __<   \ \ \-.  \  
             \ \_____\  \ \_____\  \ \_\ \_\  \ \_\\"\_\ 
              \/_____/   \/_____/   \/_/ /_/   \/_/ \/_/ 
                                             
*/

//*****************************************************************************
//
//! \addtogroup entry_point
//! @{
//!
//! entry_point main() function to call setup routines.
//
//*****************************************************************************

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "inc/hw_types.h"
#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h"
#include "driverlib/uart.h"
#include "rit128x96x4.h"
#include "led.h"
#include "init.h"
#include "kernel.h"
#include "locking.h"

//*****************************************************************************
//
//! \brief Entry point for kernel
//!
//! Main function calls setup routines to initialize threads and start
//! the kernel.
//!
//! \return None.
//
//*****************************************************************************
void main(void)
{
    // Set the clocking to run directly from the crystal.
    SysCtlClockSet(SYSCTL_SYSDIV_1 | SYSCTL_USE_OSC | SYSCTL_OSC_MAIN |
                   SYSCTL_XTAL_8MHZ);

    // Initialize the OLED display
    DisplayConfig();

    // Initialize UART0 @ 115200bps, 8-N-1
    // Initialize UART1 @ 230400bps, 8-N-1
    UARTConfig(115200);

    // Initialize status LED
    initStatusLED();

    // Initialize buttons
    ButtonConfig();

    iprintf("\r\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");

    lockInitialize(&UARTLock0);
    lockInitialize(&UARTLock1);
    lockInitialize(&OLEDLock);

    // Create all the threads and allocate a stack for each one
    if(createThreads())
    {
        iprintf("Thread creation successful!\r\n");
        RIT128x96x4StringDraw("Threads Created!", 0,  8, 15);
    }
    else
    {
        iprintf("Thread creation failed!\r\n");
        RIT128x96x4StringDraw("Create FAIL!", 0,  8, 15);
        exit(0);
    }

    Timer0Config();
    SysTickConfig();

    iprintf("Starting Kernel!\r\n");
    startKernel();

    // If scheduler() returns, all coroutines are inactive and we return
    // from main() hence exit() should be called implicitly (according to
    // ANSI C). However, TI's startup_gcc.c code (ResetISR) does not
    // call exit() so we do it manually.
    exit(0);
}

//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************
