/*    
    EGR 424
    Project 4 - Multithreaded Kernel
    Prof. Sterian && Prof. Parikh
    August 7, 2012
    
    STARRING:
     ______   ______     __     ______     __   __     _____    
    /\  ___\ /\  == \   /\ \   /\  ___\   /\ "-.\ \   /\  __-.  
    \ \  __\ \ \  __<   \ \ \  \ \  __\   \ \ \-.  \  \ \ \/\ \ 
     \ \_\    \ \_\ \_\  \ \_\  \ \_____\  \ \_\\"\_\  \ \____- 
      \/_/     \/_/ /_/   \/_/   \/_____/   \/_/ \/_/   \/____/ 

                            --- AND ---
             ______     ______     ______     __   __                             
            /\  == \   /\  __ \   /\  == \   /\ "-.\ \   
            \ \  __<   \ \ \/\ \  \ \  __<   \ \ \-.  \  
             \ \_____\  \ \_____\  \ \_\ \_\  \ \_\\"\_\ 
              \/_____/   \/_____/   \/_/ /_/   \/_/ \/_/ 
                                             
*/

//*****************************************************************************
//
//! \file kernel.h
//!
//! Header for kernel.c containing api function delcarations and variables.
//
//*****************************************************************************

#ifndef KERNEL_H
#define KERNEL_H

#include "threads.h"
#include "inc/hw_types.h"
#include "ringbuffer.h"

//*****************************************************************************
//
// Define NULL, if not already defined.
//
//*****************************************************************************
#ifndef NULL
#define NULL        ((void *)0)
#endif

#ifndef TRUE
#define TRUE        1
#endif
#ifndef FALSE
#define FALSE       0
#endif

// Display size macros
//! \def
#define DISPLAY_XPIX    128
//! \def
#define DISPLAY_YPIX    96

// Definitions of control characters
//! \def
#define XON     17
//! \def
#define XOFF    19
//! \def
#define START   0x01

// Display is 4 bit grayscale so there are 4 bits per pixel
//! \def
#define PIX_PER_BYTE    2

// Uncomment to enable grayscale mode
#define GRAY

//! \def
#define STACK_SIZE 2048     // Amount of stack space for each thread

//! \def
#define MAX_CHANNELS 32     // Maximum ammount of message channels

// Need to save 10 additional registers to completely preserve the
// state of a thread.
//! \brief Register buffer declaration. Stores ``r4-r12`` and ``lr``.
typedef long int registerBuffer[10];

//*****************************************************************************
//
//! Holds information about a thread.
//!
//! - \b state Indicates the state of the thread (DEAD, AWAKE, SLEEPING)
//! - \b wakeTime indicates when the thread should be woken up. Time is
//!      specified as the target value of gulMSTick in milliseconds.
//! - \b stack The stack pointer for the process.
//! - \b savedRegisters A registerBuffer to capture the state of a thread
//!      during a context switch.
//! - \b subscriptions The channesl that a thread is subscribed to. A thread
//!      can subscribe to 32 channels (1 bit per channel).
//! - \b messages Ring buffers for messages on each channel.
//
//*****************************************************************************
typedef struct {
    unsigned long state;
    unsigned long wakeTime;
    char *stack;
    registerBuffer savedRegisters;
    unsigned long subscriptions;
    ringBuffer_t messages[MAX_CHANNELS];
} threadStruct_t;

// Thread state macro defs
#define DEAD        0
#define AWAKE       1
#define SLEEPING    2

//! \brief User-space thread type declaration. Takes no parameters and has no return value.
typedef void (*thread_t)(void);

//! \var
//! \brief Contains pointers to thread functions
extern thread_t threadTable[];

//! \def
//! \brief Number of threads in the system
#define NUM_THREADS (sizeof(threadTable)/sizeof(threadTable[0]))

//! \var 
//! \brief The index of the currently running thread
extern int gCurrentThread;

//! \var
//! \brief Global millisecond tick
extern unsigned long gulMSTick;

//! \var
//! \brief Contains the thread data
extern threadStruct_t threads[];

void clearStack(char *stackptr);
int checkStackFreeSpace(char *stackptr);
void yield(void);
void initThread(registerBuffer buffer, char *stack);
void threadStarter(void);
void threadDestroyer(void);
int createThreads(void);
int getNumThreads(void);
void scheduler(void);
void startKernel(void);

#endif