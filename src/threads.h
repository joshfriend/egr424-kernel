/*    
    EGR 424
    Project 4 - Multithreaded Kernel
    Prof. Sterian && Prof. Parikh
    August 7, 2012
    
    STARRING:
     ______   ______     __     ______     __   __     _____    
    /\  ___\ /\  == \   /\ \   /\  ___\   /\ "-.\ \   /\  __-.  
    \ \  __\ \ \  __<   \ \ \  \ \  __\   \ \ \-.  \  \ \ \/\ \ 
     \ \_\    \ \_\ \_\  \ \_\  \ \_____\  \ \_\\"\_\  \ \____- 
      \/_/     \/_/ /_/   \/_/   \/_____/   \/_/ \/_/   \/____/ 

                            --- AND ---
             ______     ______     ______     __   __                             
            /\  == \   /\  __ \   /\  == \   /\ "-.\ \   
            \ \  __<   \ \ \/\ \  \ \  __<   \ \ \-.  \  
             \ \_____\  \ \_____\  \ \_\ \_\  \ \_\\"\_\ 
              \/_____/   \/_____/   \/_/ /_/   \/_/ \/_/ 
                                             
*/

//*****************************************************************************
//
//! \file threads.h
//!
//! Header for threads.c containing api function delcarations and variables.
//
//*****************************************************************************

#ifndef THREADS_H
#define THREADS_H

//! \addtogroup threads
//! @{
//! \def
//! \brief Macro def for thread function type
#define THREAD(name) void name(void)
//! @}

THREAD(thread0);
THREAD(thread1);
THREAD(thread2);
THREAD(checkSelectButton);
THREAD(idleThread);
THREAD(blinkThread);
THREAD(OLEDThread);
THREAD(privilegeTest);
THREAD(contextSwitchTimeTester);
THREAD(publishThread);
THREAD(recieveAllThread);
THREAD(drawImageThread);

#endif