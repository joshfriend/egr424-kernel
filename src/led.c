/*    
    EGR 424
    Project 4 - Multithreaded Kernel
    Prof. Sterian && Prof. Parikh
    August 7, 2012
    
    STARRING:
     ______   ______     __     ______     __   __     _____    
    /\  ___\ /\  == \   /\ \   /\  ___\   /\ "-.\ \   /\  __-.  
    \ \  __\ \ \  __<   \ \ \  \ \  __\   \ \ \-.  \  \ \ \/\ \ 
     \ \_\    \ \_\ \_\  \ \_\  \ \_____\  \ \_\\"\_\  \ \____- 
      \/_/     \/_/ /_/   \/_/   \/_____/   \/_/ \/_/   \/____/ 

                            --- AND ---
             ______     ______     ______     __   __                             
            /\  == \   /\  __ \   /\  == \   /\ "-.\ \   
            \ \  __<   \ \ \/\ \  \ \  __<   \ \ \-.  \  
             \ \_____\  \ \_____\  \ \_\ \_\  \ \_\\"\_\ 
              \/_____/   \/_____/   \/_/ /_/   \/_/ \/_/ 
                                             
*/

#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"

//*****************************************************************************
//
//! \addtogroup led_api
//! @{
//!
//! Led_api contains functions for initializing and controlling the LED.
//
//*****************************************************************************

//*****************************************************************************
//
//! \brief Enable the onboard Status LED
//!
//! \return None.
//
//*****************************************************************************
void initStatusLED(void)
{
    // Enable the GPIO port that is used for the on-board LED.
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_0);
}

//*****************************************************************************
//
//! \brief Toggle the onboard status LED
//!
//! \return None.
//
//*****************************************************************************
void toggleStatusLED(void)
{
    GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_0,
                 !GPIOPinRead(GPIO_PORTF_BASE, (GPIO_PIN_0)));
    // GPIO_PORTF_DATA_R ^= 0x01;
}

//*****************************************************************************
//
//! \brief Set the state of the onboard status LED
//!
//! \return None.
//
//*****************************************************************************
void setStatusLED(unsigned char en)
{
    GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_0, en);
}

//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************