/*    
    EGR 424
    Project 4 - Multithreaded Kernel
    Prof. Sterian && Prof. Parikh
    August 7, 2012
    
    STARRING:
     ______   ______     __     ______     __   __     _____    
    /\  ___\ /\  == \   /\ \   /\  ___\   /\ "-.\ \   /\  __-.  
    \ \  __\ \ \  __<   \ \ \  \ \  __\   \ \ \-.  \  \ \ \/\ \ 
     \ \_\    \ \_\ \_\  \ \_\  \ \_____\  \ \_\\"\_\  \ \____- 
      \/_/     \/_/ /_/   \/_/   \/_____/   \/_/ \/_/   \/____/ 

                            --- AND ---
             ______     ______     ______     __   __                             
            /\  == \   /\  __ \   /\  == \   /\ "-.\ \   
            \ \  __<   \ \ \/\ \  \ \  __<   \ \ \-.  \  
             \ \_____\  \ \_____\  \ \_\ \_\  \ \_\\"\_\ 
              \/_____/   \/_____/   \/_/ /_/   \/_/ \/_/ 
                                             
*/

#include "locking.h"
#include "kernel.h"
#include <stdio.h>

//*****************************************************************************
//
//! \addtogroup locking_api
//! @{
//!
//! Locking_api includes functions for initializing, acquiring and releasing
//! locks on peripherals.
//
//*****************************************************************************


//*****************************************************************************
//
//! \brief Initialize a lock
//!
//! Set initial values for a lock_t object.
//!
//! \return None.
//
//*****************************************************************************
void lockInitialize(lock_t *lock)
{
    lock->unlocked = 1;
    lock->lockedByThread = -1;
    lock->count = 0;
}

//*****************************************************************************
//
//! \internal
//! \brief Attempt to acquire a lock
//!
//! Attempt to acquire a lock. Uses LDREX and STREX special instructions.
//!
//! \return None.
//
//*****************************************************************************
static unsigned lockAcquireTry(lock_t *lock)
{
    unsigned success;
    unsigned scratch = 0;

    asm volatile(
        "   mov     r1, #0                              \n\t"   // lockedVal = 0;
        "   ldrex   %[scratch], [%[isunlocked]]         \n\t"   // isUnlocked = lock.unlocked
        "   cmp     %[scratch], r1                      \n\t"   // if(isUnlocked)
        "   itt     ne                                  \n\t"   // {
        "   strexne %[scratch], r1, [%[isunlocked]]     \n\t"   //      lock.unlocked = 0;
        "   cmpne   %[scratch], #1                      \n\t"   //      if(lockFailure)
                                                                //      {
        "   beq     bail                                \n\t"   //          bail();
                                                                //      }
                                                                //      else
                                                                //      {
        "   str     %[thread], [%[lockedByThread]]      \n\t"   //          lock.lockedByThread = gCurrentThread;
        "   mov     %[success], #1                      \n\t"   //          success = 1;
        "   b       done                                \n\t"   //          return 1;
                                                                //      }
                                                                // }
        "bail:                                          \n\t"
        "   clrex                                       \n\t"   // clearAccessAttempt();
        "   mov     %[success], #0                      \n\t"   // success = 0;

        "done:                                          \n\t"   // Empty label to branch to

        : [success] "=r" (success)
        : [scratch] "r" (scratch),
          [isunlocked] "r" (&lock->unlocked),
          [lockedByThread] "r" (&lock->lockedByThread),
          [thread] "r" (gCurrentThread)
        : "r1"
    );
    return success;
}

//*****************************************************************************
//
//! \brief Acquire a lock
//!
//! Checks if lock is taken already, takes lock if free, yield()s if not.
//!
//! \return None.
//
//*****************************************************************************
void lockAcquire(lock_t *lock)
{
    // Check if lock is already acquired
    if(lock->lockedByThread != gCurrentThread)
    {
        while(!lockAcquireTry(lock))
        {
            // Can't do anything without lock so yield to other processes
            yield();
        }
    }
    lock->count++;
}

//*****************************************************************************
//
//! \brief Release a lock
//!
//! Checks if current thread has permission to release a lock, then releases it
//! if lockRelease() has been called as many times as lockAcquire().
//!
//! \return Unlock status (\b 1 if succeeded, \b 0 if not).
//
//*****************************************************************************
unsigned lockRelease(lock_t *lock)
{
    unsigned success = 0;

    // Only unlock if current thread is unlocking
    if(lock->lockedByThread == gCurrentThread)
    {
        // Decrement lock count and check if completely unlocked
        if(!(--(lock->count)))
        {
            lock->lockedByThread = -1;
            lock->unlocked = 1;
            success = 1;
        }
    }

    return success;
}

//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************