/*    
    EGR 424
    Project 4 - Multithreaded Kernel
    Prof. Sterian && Prof. Parikh
    August 7, 2012
    
    STARRING:
     ______   ______     __     ______     __   __     _____    
    /\  ___\ /\  == \   /\ \   /\  ___\   /\ "-.\ \   /\  __-.  
    \ \  __\ \ \  __<   \ \ \  \ \  __\   \ \ \-.  \  \ \ \/\ \ 
     \ \_\    \ \_\ \_\  \ \_\  \ \_____\  \ \_\\"\_\  \ \____- 
      \/_/     \/_/ /_/   \/_/   \/_____/   \/_/ \/_/   \/____/ 

                            --- AND ---
             ______     ______     ______     __   __                             
            /\  == \   /\  __ \   /\  == \   /\ "-.\ \   
            \ \  __<   \ \ \/\ \  \ \  __<   \ \ \-.  \  
             \ \_____\  \ \_____\  \ \_\ \_\  \ \_\\"\_\ 
              \/_____/   \/_____/   \/_/ /_/   \/_/ \/_/ 
                                             
*/

//*****************************************************************************
//
//! \file locking.h
//!
//! Header for locking.c containing api function delcarations and variables.
//
//*****************************************************************************

#ifndef LOCKING_H
#define LOCKING_H

//*****************************************************************************
//
//! Holds information about a lock.
//!
//! - \b unlocked Indicates if the thread is unlocked (1) or not (0).
//! - \b lockedByThread The thread that aquired the lock.
//
//*****************************************************************************
typedef struct
{
    int unlocked;
    int lockedByThread;
    int count;
} lock_t;

// GPIO locks

//! \addtogroup GPIO_Locks
// @{
//! \brief lock_t for GPIO Port A
lock_t GPIOLockA;
//! \brief lock_t for GPIO Port B
lock_t GPIOLockB;
//! \brief lock_t for GPIO Port C
lock_t GPIOLockC;
//! \brief lock_t for GPIO Port D
lock_t GPIOLockD;
//! \brief lock_t for GPIO Port E
lock_t GPIOLockE;
//! \brief lock_t for GPIO Port F
lock_t GPIOLockF;
//! \brief lock_t for GPIO Port G
lock_t GPIOLockG;
// @}

// OLED peripherals
//! \addtogroup OLED_Locks
// @{
//! \brief lock_t for OLED Screen
lock_t OLEDLock;
// @}

// UART peripherals
//! \addtogroup UART_Locks
// @{
//! \var UARTLock1
//! \brief lock_t for UART0
lock_t UARTLock0;
//! \brief lock_t for UART1
lock_t UARTLock1;
//! \brief lock_t for UART2
lock_t UARTLock2;
// @}

void lockInitialize(lock_t *lock);
static unsigned lockAcquireTry(lock_t *lock);
void lockAcquire(lock_t *lock);
unsigned lockRelease(lock_t *lock);

#endif /* LOCKING_H */
