/*    
    EGR 424
    Project 4 - Multithreaded Kernel
    Prof. Sterian && Prof. Parikh
    August 7, 2012
    
    STARRING:
     ______   ______     __     ______     __   __     _____    
    /\  ___\ /\  == \   /\ \   /\  ___\   /\ "-.\ \   /\  __-.  
    \ \  __\ \ \  __<   \ \ \  \ \  __\   \ \ \-.  \  \ \ \/\ \ 
     \ \_\    \ \_\ \_\  \ \_\  \ \_____\  \ \_\\"\_\  \ \____- 
      \/_/     \/_/ /_/   \/_/   \/_____/   \/_/ \/_/   \/____/ 

                            --- AND ---
             ______     ______     ______     __   __                             
            /\  == \   /\  __ \   /\  == \   /\ "-.\ \   
            \ \  __<   \ \ \/\ \  \ \  __<   \ \ \-.  \  
             \ \_____\  \ \_____\  \ \_\ \_\  \ \_\\"\_\ 
              \/_____/   \/_____/   \/_/ /_/   \/_/ \/_/ 
                                             
*/

//*****************************************************************************
//
//! \file ringbuffer.h
//!
//! Header for kernel.c containing api function delcarations and variables.
//
//*****************************************************************************

#ifndef RINGBUFFER_H
#define RINGBUFFER_H

#include "inc/hw_types.h"
#include "locking.h"

//! \def
#define BUF_SIZE    8

//*****************************************************************************
//
//! Read/Write pointers and lock data for a ring buffer.
//!
//! - \b pulBuffer Pointer to the buffer used by the ringBuffer_t object.
//! - \b size Number of array elements in the buffer
//! - \b ulRead The read index of the ringBuffer_t object.
//! - \b ulWrite The write index of the ringBuffer_t object.
//! - \b lock The lock_t object used to prevent errors from simultaneous
//!      read/write operations in different threads.
//
//*****************************************************************************
typedef struct {
    unsigned long ulBuffer[BUF_SIZE];
    unsigned long size;
    unsigned long ulRead;
    unsigned long ulWrite;
    lock_t lock;
} ringBuffer_t;

//! \def
#define FAILURE     0
//! \def
#define SUCCESS     1

//! \def
#define TRUE        1
//! \def
#define FALSE       0

extern int createRingBuffer(ringBuffer_t * ptRingBuf);
extern int ringBufferWrite(ringBuffer_t * ptRingBuf, unsigned long value);
extern int ringBufferRead(ringBuffer_t * ptRingBuf, unsigned long *value);
extern int isRingBufferEmpty(ringBuffer_t * ptRingBuf);
extern int isRingBufferFull(ringBuffer_t * ptRingBuf);

#endif