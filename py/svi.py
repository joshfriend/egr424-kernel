#!/usr/bin/env python

#     EGR 424
#     Project 2 - Serial Video Interface
#     Prof. Sterian && Prof. Parikh
#     June 21, 2012
#
#     STARRING:
#      ______   ______     __     ______     __   __     _____
#     /\  ___\ /\  == \   /\ \   /\  ___\   /\ "-.\ \   /\  __-.
#     \ \  __\ \ \  __<   \ \ \  \ \  __\   \ \ \-.  \  \ \ \/\ \
#      \ \_\    \ \_\ \_\  \ \_\  \ \_____\  \ \_\\"\_\  \ \____-
#       \/_/     \/_/ /_/   \/_/   \/_____/   \/_/ \/_/   \/____/
#
#                             --- AND ---
#              ______     ______     ______     __   __
#             /\  == \   /\  __ \   /\  == \   /\ "-.\ \
#             \ \  __<   \ \ \/\ \  \ \  __<   \ \ \-.  \
#              \ \_____\  \ \_____\  \ \_\ \_\  \ \_\\"\_\
#               \/_____/   \/_____/   \/_/ /_/   \/_/ \/_/

try:
    # Builtin
    import argparse
    import os.path
    import os
    import sys
    import time
    # Addin modules
    import serial
    from serial.tools.list_ports import comports
    # Local modules
    import imageparse
except ImportError as e:
    sys.exit('---> Additional modules required!\n%s' % e)


def main(device, baudrate, path, verbose):
    # Parse image data
    image_frames = imageparse.parse_image(path, 0x01)
    print '---> Opening port "%s" at %s bps...' % (device, baudrate)
    print '---> Press CTRL + C to interrupt...'

    # Instantiate serial port and set properties
    port = serial.Serial()
    port.port = device                      # Set port
    port.baudrate = baudrate                # Set baudrate
    port.parity = serial.PARITY_NONE        # Configure for 8-N-1 mode
    port.bytesize = serial.EIGHTBITS
    port.stopbits = serial.STOPBITS_ONE
    port.timeout = 0                        # Reads from port should be non-blocking
    port.writeTimeout = 0                   # Writes to port should be non-blocking
    port.interCharTimeout = None
    port.xonxoff = False                    # Using XON/XOFF manually since this locks for some reason
    port.rtscts = False                     # Hardware flow control is not supported
    port.dsrdtr = False

    # For fancy printing to console
    if sys.platform == 'darwin':
        rows, columns = os.popen('stty size', 'r').read().split()
        fill = " " * int(columns)

    nframes = len(image_frames)
    trials = []

    try:
        port.open()

        # Block till XON signal recieved
        while port.read(1) is not serial.XON:
            sys.stdout.write('---> Waiting for XON...\r')
            sys.stdout.flush()
            time.sleep(0.01)
        print '---> XON recieved!     '

        # Poll the reciever device
        port.write(serial.XON)
        port.flush()

        while True:
            frame_start = time.time()

            for n in xrange(nframes):
                bytes = 0
                line_start = time.time()
                for line in image_frames[n]:
                    port.write(line)
                    port.flush()
                    v = port.read(1)
                    if v is serial.XOFF:
                        # Wait for device to finish processing
                        while port.read(1) is not serial.XON:
                            time.sleep(0.001)

                    # Track number of bytes sent
                    bytes += len(line)

                if verbose:
                    sys.stdout.write('---> Frame %s of %s (%s Bytes, %.3f sec)' % (n + 1, nframes, bytes, time.time() - line_start))
                    sys.stdout.write('\r')
                    if sys.platform is 'darwin':
                        sys.stdout.write(fill + '\r')
                    sys.stdout.flush()

            tm = (time.time() - frame_start)

            if verbose:
                print '---> Time taken: %-5.2fms @ %.3ffps           ' % (tm * 1000, nframes / tm)
            trials.append(nframes / tm)

    except KeyboardInterrupt:
        if sys.platform == 'darwin':
            sys.stdout.write('\r' + fill + '\r')
        sys.stdout.flush()
        if verbose:
            if len(trials) > 0:
                print '---> Average framerate: %-.3f fps over %i transmissions.' % (sum(trials) / len(trials), len(trials))
        print '---> Quitting...'

    port.close()


def dump_port_list():
    """Returns a list of available serial ports"""
    available = True
    ports = []
    portnum = 1
    if comports:
        print '---> Available ports:'
        for port, desc, hwid in sorted(comports()):
            ports.append(port)
            #~ sys.stderr.write('--- %-20s %s [%s]\n' % (port, desc, hwid))
            print '%3i.)\t%s' % (portnum, port)
            portnum += 1
    else:
        print '---> No serial ports found!'
        available = False

    return (available, ports)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='EGR424 Project 2 S.V.I. (Serial Video Interface)')
    parser.add_argument('-d', action='store', dest='device', help='Device name (eg. /dev/tty.usbserial-ABCDXYZ01)')
    parser.add_argument('-b', action='store', dest='baudrate', type=int, default=230400, help='Baudrate (default = 230400 bps)')
    parser.add_argument('-f', action='store', dest='path', required=True, help='Input file location')
    parser.add_argument('-v', action='store_true', help='Enable verbose output (framerate, timing, etc...)')
    parser.add_argument('--version', action='version', version='EGR424 S.V.I 2.0 - Authors: Eric Born and Josh Friend')
    args = parser.parse_args()

    error = False

    # Verify correct baudrate
    if args.baudrate not in serial.Serial.BAUDRATES:
        print '---> Invalid baudrate: %s' % args.baudrate
        print '---> Valid baudrates:'
        print ', '.join('%s' % b for b in serial.Serial.BAUDRATES)
        error = True

    # Parse device
    if not error:
        if args.device is None:
            (available, ports) = dump_port_list()
            if not available:
                print '---> Unable to find any compatible devices!'
                error = True
            else:
                print '---> Enter devce number:',
                try:
                    selection = int(raw_input().rstrip('\n')) - 1
                    if selection not in range(len(ports)):
                        print '---> Invalid device number: "%s"' % selection + 1
                        error = True
                    else:
                        args.device = ports[selection]
                except ValueError:
                    print '---> Not a number!'
                    error = True
        elif not os.path.exists(args.device):
            print '---> Invalid device name: "%s"' % args.device
            error = True

    # Validate input file
    if not error:
        if os.path.exists(args.path):
            print '---> Input file valid!'
        else:
            print '---> Input file not found: %s' % os.path.abspath(args.path)
            error = True

    if not error:
        main(args.device, args.baudrate, os.path.abspath(args.path), args.v)
