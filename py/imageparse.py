#!/usr/bin/env python

#     Project 2 - Serial Video Interface
#     Prof. Sterian && Prof. Parikh
#     June 21, 2012
#
#     STARRING:
#      ______   ______     __     ______     __   __     _____
#     /\  ___\ /\  == \   /\ \   /\  ___\   /\ "-.\ \   /\  __-.
#     \ \  __\ \ \  __<   \ \ \  \ \  __\   \ \ \-.  \  \ \ \/\ \
#      \ \_\    \ \_\ \_\  \ \_\  \ \_____\  \ \_\\"\_\  \ \____-
#       \/_/     \/_/ /_/   \/_/   \/_____/   \/_/ \/_/   \/____/
#
#                             --- AND ---
#              ______     ______     ______     __   __
#             /\  == \   /\  __ \   /\  == \   /\ "-.\ \
#             \ \  __<   \ \ \/\ \  \ \  __<   \ \ \-.  \
#              \ \_____\  \ \_____\  \ \_\ \_\  \ \_\\"\_\
#               \/_____/   \/_____/   \/_/ /_/   \/_/ \/_/

try:
    # Builtin
    import sys
    # Addin modules
    from PIL import Image
except ImportError as e:
    sys.exit('---> Additional modules required!\n' + e)


def parse_image(filepath, markln):
    """Converts an animated GIF into a nested list"""

    im = Image.open(filepath)
    img_type = im.tile[0][3][0]
    frames = None
    if im.tile[0][0] == "gif":
        # Get image data
        tag, (x0, y0, x1, y1), offset, extra = im.tile[0]
        # Get size
        im.size = x1 - x0, y1 - y0

        frames = []
        nframes = 0
        try:
            print '---> Parsing frames ',
            while True:
                sys.stdout.write(".")
                # Get the pixel values for a frame in a single list
                data = list(im.getdata())
                frame = []

                # Split the single list into pixel row/column lists
                for n in xrange(im.size[1]):
                    # Retrieve one row of pixels
                    pixel_line = list(data[n * im.size[0]:(n + 1) * im.size[0]])

                    line = bytearray()
                    if img_type is 4:
                        # Image is Grayscale (4bit gray depth)
                        for i in xrange(0, len(pixel_line), 2):
                            val = (pixel_line[i] << 4) | (pixel_line[i + 1])
                            if val in [markln]:
                                val += 1
                            line.append(val)
                    else:
                        # Image is Black & White (1 bit depth)
                        for i in xrange(len(pixel_line) / 8):
                            # Grab line data 1 byte at a time
                            byte_list = list(pixel_line[i * 8:(i + 1) * 8])

                            val = 0
                            for j in xrange(8):
                                val |= (byte_list[j] << j)
                            if val in [markln]:
                                val += 1
                            line.append(val)

                    # Prepend control characters to line
                    line.insert(0, n)
                    line.insert(0, markln)
                    frame.append(line)

                frames.append(frame)

                # Advance frame count
                im.seek(im.tell() + 1)
                nframes += 1
        except EOFError:
            # End of sequence
            print ' DONE!'
            if img_type is 4:
                print '---> Color depth: Grayscale'
            else:
                print '---> Color depth: Black & White'
            print '---> Image size: %sx%s pixels, %s Frames...' % (im.size[0], im.size[1], nframes + 1)
    else:
        print '---> Wrong image type: %s' % img_type

    return frames


def verify(image, markln):
    """Scan parsed data for errors in parsing"""
    valid = True
    for frame in image:
        for line in frame:
            for pixel in xrange(len(line)):
                if line[pixel] in [markln] and pixel not in [0, 1]:
                    valid = False
    return valid


def write_file(path, image):
    """Writes the image data to a file"""
    fp = open(path, 'w')
    for frame in image:
        for line in frame:
            fp.write(line)
    fp.close()


def print_image(image):
    """Prints image data to console"""
    for frame in image:
        print '{',
        for line in frame:
            print '{' + ', '.join(['0x%02X' % n for n in line]) + '},'
        print '}\n\n'


if __name__ == "__main__":
    # perform tests of methods in module
    from path import path
    frames = parse_image(path(__file__).dirname() / '..' / 'beatles_abbey_road.gif', 0x01)
    print '---> Parsed data valid?', verify(frames, 0x01)
    write_file('../beatles_abbey_road.txt', frames)
    print_image(frames)
