# Serial Video Interface (SVI)

Decodes a Black & White or Grayscale GIF image and sends the data to a slave device via serial

## Authors
---

* **_Eric Born_**
* **_Josh Friend_**

## Requirements
---

These additional packages can be installed using [easy_install](http://pypi.python.org/pypi/setuptools) or [pip](http://www.pip-installer.org/en/latest/index.html)

* PySerial
* PIL
* path.py
* argparse (not included in Python 2.6 or below)

##Using the Python application
---
Run the python app as follows:

`python svi.py -f <filename>`

You can also specify a baudrate (default is 115200 bps):

`python svi.py -f <filename> -b  <baudrate>`

Specifying the `-v` flag enables verbose output (framerate, frame transmission time, bytes sent):

`python svi.py -f <filename> -b  <baudrate> -v`

Quit the application by pressing `CTRL + C`

##Limitations
---
While the python application itself can support various image resolutions, the associated software for the ARM devkit requires 128x96 resolution images.
